<?php

class CRM_Member_MembershipPeriod {
  private static $before;

  static public function postMembershipCreate ($id, &$membership) {
    self::postMemberEdit($id, $membership);
  }

  static public function postMembershipEdit ($id, $membership) {
    $unaltered = &$before[$id];

    $old_start_date = strtotime($unaltered->start_date);
    $old_end_date   = strtotime($unaltered->end_date);
    $new_start_date = strtotime($membership->start_date);
    $new_end_date   = strtotime($membership->end_date);

    // @TODO: load the membership type for term length, etc.
    
    if ($new_end_date > $old_end_date) {
      // Calculate terms to insert.
    }
  }

  // Store existing membership details before membership is saved.
  static public function preMembershipEdit ($id, &$params) {
    $membership = new CRM_Member_BAO_Membership();
    $membership->id = $id;
    $membership->find(TRUE);
    
    $before[$id] = &$membership;
  }
}