<?php
// This file declares a managed database record of type "ReportTemplate".
// The record will be automatically inserted, updated, or deleted from the
// database as appropriate. For more details, see "hook_civicrm_managed" at:
// http://wiki.civicrm.org/confluence/display/CRMDOC42/Hook+Reference
return array (
  0 => 
  array (
    'name' => 'CRM_Member_Form_Report_MembershipPeriod',
    'entity' => 'ReportTemplate',
    'params' => 
    array (
      'version' => 3,
      'label' => 'Membership Period Report',
      'description' => 'Provides reporting on entries for individual Membership periods (au.com.agileware.membershipperiod)',
      'class_name' => 'CRM_Member_Form_Report_MembershipPeriod',
      'report_url' => 'au.com.agileware.membershiplogext/membership-period',
      'component' => 'CiviMember',
    ),
  ),
);